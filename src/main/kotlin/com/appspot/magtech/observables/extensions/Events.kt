package com.appspot.magtech.observables.extensions

import kotlin.reflect.KFunction
import kotlin.reflect.KMutableProperty0

fun KFunction<*>.event(vararg target: KMutableProperty0<*>) {
    target.forEach {
        it.addListener {
            this@event.call()
        }
    }
}

fun <T> KFunction<*>.event(target: KMutableProperty0<T>) = event(target) {true}

fun <T> KFunction<*>.event(target: KMutableProperty0<T>, predicate: (T) -> Boolean) {
    target.addListener {
        if (predicate(it)) {
            this@event.call()
        }
    }
}