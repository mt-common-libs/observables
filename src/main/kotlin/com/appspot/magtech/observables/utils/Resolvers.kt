package com.appspot.magtech.observables.utils

import com.appspot.magtech.observables.delegates.ObservableVar
import kotlin.reflect.KClass
import kotlin.reflect.KMutableProperty0
import kotlin.reflect.KProperty
import kotlin.reflect.full.starProjectedType
import kotlin.reflect.jvm.jvmErasure

private class Resolver<T>(init: T) {

    var property: T by ObservableVar(init)
}

private val resolverStore = arrayListOf<Resolver<*>>()



@Suppress("UNCHECKED_CAST")
fun <T> resolveByName(obj: Any, propName: String): KMutableProperty0<T> {
    val delegatedPropsField = obj::class.java.getDeclaredField("\$\$delegatedProperties")
    delegatedPropsField.isAccessible = true
    val delegatedProps = delegatedPropsField.get(obj) as Array<KProperty<*>>
    val delegatedProp = delegatedProps.find { it.name == propName }!!
    delegatedPropsField.isAccessible = false

    val resolver = Resolver(delegatedProp.getter.call(obj) as T)
    val delegateField = obj::class.java.getDeclaredField("$propName\$delegate")
    delegateField.isAccessible = true
    val delegate = delegateField.get(obj) as ObservableVar<T>
    delegate.listeners += {
        resolver.property = it
    }
    delegateField.isAccessible = false

    resolverStore.add(resolver)
    return resolver::property
}

@Suppress("UNCHECKED_CAST")
fun <T: Any> resolveByType(obj: Any, type: KClass<out T>): KMutableProperty0<T> {
    val delegatedPropsField = obj::class.java.getDeclaredField("\$\$delegatedProperties")
    delegatedPropsField.isAccessible = true
    val delegatedProps = delegatedPropsField.get(obj) as Array<KProperty<*>>
    val delegatedProp = delegatedProps.find {
        it.returnType.jvmErasure == type.starProjectedType.jvmErasure
    }!!
    delegatedPropsField.isAccessible = false
    return resolveByName(obj, delegatedProp.name)
}

inline fun <reified T: Any> resolveByType(obj: Any): KMutableProperty0<T> {
    return resolveByType(obj, T::class)
}

infix fun <T> KMutableProperty0<T>.resolveBy(obj: Any): KMutableProperty0<T> {
    return resolveByName(obj, this.name)
}