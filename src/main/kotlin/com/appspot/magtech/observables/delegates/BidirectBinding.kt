package com.appspot.magtech.observables.delegates

import com.appspot.magtech.observables.extensions.getObservableVarDelegate
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KMutableProperty0
import kotlin.reflect.KProperty

class BidirectBinding<F, T>(private val target: KMutableProperty0<T>,
                            private val fromTarget: (T) -> F,
                            private val toTarget: (F) -> T): ReadWriteProperty<Any?, F> {

    private val valDelegate = ObservableVar(fromTarget(target.get()))
    private var value by valDelegate

    init {
        val delegate = target.getObservableVarDelegate()
        delegate.listeners += {
            if (value != fromTarget(it)) {
                value = fromTarget(it)
            }
        }

        valDelegate.listeners += {
            if (target.get() != toTarget(it)) {
                target.set(toTarget(it))
            }
        }
    }

    override fun getValue(thisRef: Any?, property: KProperty<*>) = value

    override fun setValue(thisRef: Any?, property: KProperty<*>, value: F) { this.value = value }
}

fun <F, T> bidirectBinding(target: KMutableProperty0<T>,
                           fromTarget: (T) -> F,
                           toTarget: (F) -> T) = BidirectBinding(target, fromTarget, toTarget)

fun <T> bidirectBinding(target: KMutableProperty0<T>) = BidirectBinding(target,
    fromTarget = {it},
    toTarget = {it})