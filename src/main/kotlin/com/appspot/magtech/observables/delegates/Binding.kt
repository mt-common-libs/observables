package com.appspot.magtech.observables.delegates

import com.appspot.magtech.observables.extensions.getObservableVarDelegate
import kotlin.properties.ReadOnlyProperty
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KMutableProperty0
import kotlin.reflect.KProperty

class Binding<F, T>(target: KMutableProperty0<T>,
                    private val fromTarget: (T) -> F): ReadOnlyProperty<Any?, F> {

    private var value: F = fromTarget(target.get())

    init {
        val delegate = target.getObservableVarDelegate()
        delegate.listeners += {
            value = fromTarget(it)
        }
    }

    override fun getValue(thisRef: Any?, property: KProperty<*>) = value
}

fun <F, T> binding(target: KMutableProperty0<T>,
                   fromTarget: (T) -> F) = Binding(target, fromTarget)

fun <T> binding(target: KMutableProperty0<T>) = Binding(target) { it }