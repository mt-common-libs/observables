package com.appspot.magtech.observables.delegates

import com.appspot.magtech.observables.extensions.Listener
import com.appspot.magtech.observables.extensions.Observer
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

class ObservableVar<T>(init: T): ReadWriteProperty<Any?, T> {

    private var value: T = init
    val listeners = arrayListOf<Listener<T>>()

    override operator fun getValue(thisRef: Any?, property: KProperty<*>): T {
        return value
    }

    override operator fun setValue(thisRef: Any?, property: KProperty<*>, value: T) {
        val prevValue = this.value
        this.value = value
        listeners.forEach {
            Observer(prevValue).it(value)
        }
    }
}