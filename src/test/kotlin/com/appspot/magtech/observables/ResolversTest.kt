package com.appspot.magtech.observables

import com.appspot.magtech.observables.delegates.ObservableVar
import com.appspot.magtech.observables.extensions.addListener
import com.appspot.magtech.observables.extensions.bind
import com.appspot.magtech.observables.extensions.event
import com.appspot.magtech.observables.utils.resolveBy
import com.appspot.magtech.observables.utils.resolveByName
import com.appspot.magtech.observables.utils.resolveByType
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class ResolversTest {

    interface Model {
        var amount: Int
        var total: Double
        var list: ArrayList<String>
    }

    class ModelImpl: Model {
        override var amount by ObservableVar(0)
        override var total by ObservableVar(0.0)
        override var list by ObservableVar(arrayListOf<String>())
    }

    @Test
    fun testListenersWithResolveByName() {
        val model: Model = ModelImpl()
        resolveByName<Int>(model, "amount").addListener {
            println("New value: $it")
        }
        model.amount = 5
    }

    @Test
    fun testListenersWithResolveByType() {
        val model: Model = ModelImpl()
        resolveByType(model, Int::class).addListener {
            println("New value: $it")
        }
        model.amount = 5

        resolveByType<ArrayList<String>>(model).addListener {
            println("New list: $it")
        }
        model.list = arrayListOf("abc", "def")
    }

    @Test
    fun testListenersWithResolveByProp() {
        val model: Model = ModelImpl()
        (model::amount resolveBy model).addListener {
            println("New value: $it")
        }
        model.amount = 5
    }

    @Test
    fun testBindingWithResolveByName() {
        val model: Model = ModelImpl()
        class Entity {
            var amount: Int = 0
            var total: Double = 0.0
        }
        val entity = Entity()

        model.amount = 3
        entity::amount.bind(resolveByName(model, "amount"))
        assertEquals(3, entity.amount)
        model.amount = 5
        assertEquals(5, entity.amount)

        model.total = 3.0
        entity::total.bind(resolveByName(model, "total"))
        assertEquals(3.0, entity.total)
        model.total = 10.0
        assertEquals(10.0, entity.total)
    }

    @Test
    fun testEventWithResolveByName() {
        val model: Model = ModelImpl()
        class Entity {
            fun action() {
                println("Action")
            }
        }
        val entity = Entity()
        entity::action.event(resolveByName<Int>(model, "amount"))
        model.amount = 10
    }
}