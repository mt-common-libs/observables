package com.appspot.magtech.observables

import com.appspot.magtech.observables.delegates.ObservableVar
import com.appspot.magtech.observables.delegates.bidirectBinding
import com.appspot.magtech.observables.delegates.binding
import com.appspot.magtech.observables.extensions.bind
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class BindingsTest {

    class Model {

        var amount by ObservableVar(0)

        var title by ObservableVar("")
    }
    private val model = Model()

    @Test
    fun testBindingBidirectionalDelegate() {
        class Entity(model: Model) {
            var amount by bidirectBinding(model::amount)

            var buffer: StringBuffer by bidirectBinding(model::title,
                                                        fromTarget = { StringBuffer(it) },
                                                        toTarget = { it.toString() })
        }
        val entity = Entity(model)

        model.amount = 5
        assertEquals(5, entity.amount)

        entity.amount = 10
        assertEquals(10, model.amount)

        model.title = "abc"
        assertEquals("abc", entity.buffer.toString())

        entity.buffer = StringBuffer("abcdef")
        assertEquals("abcdef", model.title)
    }

    @Test
    fun testBindingDelegate() {
        class SecondEntity(model: Model) {
            val amount by binding(model::amount)

            val size by binding(model::title) { it.length }
        }
        val entity = SecondEntity(model)

        model.amount = 5
        assertEquals(5, entity.amount)

        model.title = "abc"
        assertEquals(3, entity.size)
    }

    @Test
    fun testBindingFun() {
        class Entity {
            var amount: Int = 0

            var total: Double = 0.0
        }
        val entity = Entity()

        entity::amount.bind(model::amount)
        model.amount = 25
        assertEquals(25, entity.amount)

        entity::total.bind(model::amount) { it.toDouble() }
        model.amount = 45
        assertEquals(45.0, entity.total)
    }

    @Test
    fun testBindingFunInPlatformClass() {
        val test = TestClass()

        model.title = "def"
        test.bind("title", model::title)
        assertEquals("def", test.title)
        model.title = "abc"
        assertEquals("abc", test.title)

        model.title = "a"
        test.bind("size", model::title) { it.length }
        assertEquals(1, test.size)
        model.title = "abcdef"
        assertEquals(6, test.size)
    }

    @Test
    fun testMultiBindingFunInPlatformClass() {
        val test = TestClass()

        model.title = "a"
        test.bind("title", "name", target = model::title)
        assertEquals("a", test.title)
        assertEquals("a", test.name)
        model.title = "def"
        assertEquals("def", test.title)
        assertEquals("def", test.name)

        model.title = "aaa"
        test.bind("size", "value", target = model::title) {
            it.count { c -> c == 'a' }
        }
        assertEquals(3, test.size)
        assertEquals(3, test.value)
        model.title = "def"
        assertEquals(0, test.size)
        assertEquals(0, test.value)
    }
}