package com.appspot.magtech.observables

import com.appspot.magtech.observables.delegates.ObservableVar
import com.appspot.magtech.observables.extensions.event
import org.junit.jupiter.api.Test

class EventsTest {

    class Model {

        var amount by ObservableVar(0)

        var title by ObservableVar("")
    }
    private val model = Model()

    @Test
    fun testEventFun() {

        class Target {

            fun play() {
                println("Executed")
            }
            fun nextAction() {
                println("Next action")
            }
        }
        val target = Target()

        target::play.event(model::amount)
        model.amount = 5

        target::nextAction.event(model::title) { it.isNotEmpty() }
        model.title = "abc"
        model.title = ""
    }

    @Test
    fun testEventFunOnPlatformClasses() {
        val test = TestClass()

        test::action.event(model::title)
        model.title = "abc"
    }
}