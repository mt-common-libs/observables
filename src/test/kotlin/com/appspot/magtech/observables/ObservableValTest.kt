package com.appspot.magtech.observables

import com.appspot.magtech.observables.delegates.ObservableVar
import com.appspot.magtech.observables.extensions.UnsupportedDelegateException
import com.appspot.magtech.observables.extensions.addListener
import com.appspot.magtech.observables.extensions.getListeners
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class ObservableValTest {

    class TestEntity {

        var amount by ObservableVar(0)

        var title by ObservableVar<String?>(null)

        var percent: Double = 0.0
    }

    @Test
    @DisplayName("Should add listeners to ObservableVar properties")
    fun addListenersTest() {
        println("Add test:")

        val entity = TestEntity()
        entity::title.addListener {
            println("New val in 1st obj: $it")
        }

        val entity2 = TestEntity()
        entity2::title.addListener {
            println("New val in 2nd obj: $it")
        }

        entity.title = "abc"
        entity2.title = "def"
    }

    @Test
    @DisplayName("should get listeners from ObservableVar property")
    fun getListenersTest() {
        println("Get test:")
        val entity = TestEntity()
        entity::amount.addListener {
            println("First Listener: $it")
        }
        entity::amount.addListener {
            println("Second Listener: $it")
        }
        entity::amount.getListeners().forEach {
            println(it)
        }
        println(entity.amount)
    }

    @Test
    @DisplayName("should throw exception if not an ObservableVar field")
    fun wrongPropsTest() {
        val entity = TestEntity()
        assertThrows<UnsupportedDelegateException> {
            entity::percent.addListener {
                println("New value: $it")
            }
        }
        assertThrows<UnsupportedDelegateException> {
            entity::percent.getListeners()
        }
    }

    @Test
    fun observersTest() {
        val entity = TestEntity()

        entity::amount.addListener {
            println("Prev int val: $prevValue; New int val: $it")
        }
        entity::title.addListener {
            println("Prev int val: $prevValue; New int val: $it")
        }

        entity.amount = 5
        entity.title = "abc"
        entity.amount = 10
    }
}