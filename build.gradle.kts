import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

buildscript {
    repositories {
        mavenLocal()
        mavenCentral()
    }
    dependencies {
        classpath("org.junit.platform:junit-platform-gradle-plugin:1.1.0")
        classpath("org.junit.platform:junit-platform-launcher:1.1.0")
    }
}

plugins {
    application
    `maven-publish`
    kotlin("jvm") version "1.3.41"
}

group = "com.appspot.magtech"
version = "2.1"

val compileKotlin: KotlinCompile by tasks
val compileTestKotlin: KotlinCompile by tasks

compileKotlin.kotlinOptions.jvmTarget = "1.8"
compileTestKotlin.kotlinOptions.jvmTarget = "1.8"

repositories {
    jcenter()
    mavenLocal()
    mavenCentral()
}

val junit_version = "5.1.0"

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation(kotlin("reflect"))

    testImplementation("org.junit.jupiter:junit-jupiter-api:$junit_version")
    testRuntime("org.junit.jupiter:junit-jupiter-engine:$junit_version")
}

val includedLibs = arrayListOf("kotlin-reflect-1.3.31.jar")

tasks.named<Jar>("jar") {
    configurations["compileClasspath"].forEach { file: File ->
        if (file.name in includedLibs) {
            from(zipTree(file.absoluteFile))
        }
    }
}

publishing {
    repositories {
        maven("https://mymavenrepo.com/repo/2adloi83ZbzFqdzrVebr/")
    }
    publications {
        create<MavenPublication>("maven") {
            groupId = "com.appspot.magtech"
            artifactId = "observables"
            version = "2.1"

            from(components["kotlin"])
        }
    }
}